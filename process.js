document.addEventListener("DOMContentLoaded", function () {
  const formularioHorarios = document.getElementById("formulario-horarios");
  const toggleHorarios = document.getElementById("toggle-horarios");
  const formularioSolicitudes = document.getElementById(
    "formulario-solicitudes"
  );
  const toggleSolicitudes = document.getElementById("toggle-solicitudes");
  const formularioProyectos = document.getElementById("formulario-proyectos");
  const toggleProyectos = document.getElementById("toggle-proyectos");
  const formularioDifusion = document.getElementById("formulario-difusion");
  const toggleDifusion = document.getElementById("toggle-difusion");

  toggleHorarios.addEventListener("click", function () {
    if (
      formularioHorarios.style.display === "none" ||
      formularioHorarios.style.display === ""
    ) {
      formularioHorarios.style.display = "block";
      formularioHorarios.style.opacity = "1";
      formularioSolicitudes.style.display = "none";
      formularioSolicitudes.style.opacity = "0";
      formularioProyectos.style.display = "none";
      formularioProyectos.style.opacity = "0";
      formularioDifusion.style.display = "none";
      formularioDifusion.style.opacity = "0";
    } else {
      formularioHorarios.style.display = "none";
      formularioHorarios.style.opacity = "0";
    }
  });

  toggleSolicitudes.addEventListener("click", function () {
    if (
      formularioSolicitudes.style.display === "none" ||
      formularioSolicitudes.style.display === ""
    ) {
      formularioSolicitudes.style.display = "block";
      formularioSolicitudes.style.opacity = "1";
      formularioHorarios.style.display = "none";
      formularioHorarios.style.opacity = "0";
      formularioProyectos.style.display = "none";
      formularioProyectos.style.opacity = "0";
      formularioDifusion.style.display = "none";
      formularioDifusion.style.opacity = "0";
    } else {
      formularioSolicitudes.style.display = "none";
      formularioSolicitudes.style.opacity = "0";
    }
  });

  toggleProyectos.addEventListener("click", function () {
    if (
      formularioProyectos.style.display === "none" ||
      formularioProyectos.style.display === ""
    ) {
      formularioProyectos.style.display = "block";
      formularioProyectos.style.opacity = "1";
      formularioHorarios.style.display = "none";
      formularioHorarios.style.opacity = "0";
      formularioSolicitudes.style.display = "none";
      formularioSolicitudes.style.opacity = "0";
      formularioDifusion.style.display = "none";
      formularioDifusion.style.opacity = "0";
    } else {
      formularioProyectos.style.display = "none";
      formularioProyectos.style.opacity = "0";
    }
  });

  toggleDifusion.addEventListener("click", function () {
    if (
      formularioDifusion.style.display === "none" ||
      formularioDifusion.style.display === ""
    ) {
      formularioDifusion.style.display = "block";
      formularioDifusion.style.opacity = "1";
      formularioHorarios.style.display = "none";
      formularioHorarios.style.opacity = "0";
      formularioSolicitudes.style.display = "none";
      formularioSolicitudes.style.opacity = "0";
      formularioProyectos.style.display = "none";
      formularioProyectos.style.opacity = "0";
    } else {
      formularioDifusion.style.display = "none";
      formularioDifusion.style.opacity = "0";
    }
  });
});
