document.addEventListener("DOMContentLoaded", function () {
  const formulario = document.querySelector("form");
  const mensaje = document.getElementById("mensaje");
  const enviarBoton = document.getElementById("enviar");

  formulario.addEventListener("submit", function (event) {
    event.preventDefault();

    alert("Mensaje enviado. ¡Gracias!");

    mensaje.value = "";

    setTimeout(function () {
      window.location.reload();
    }, 1000);
  });
});
