document.addEventListener("DOMContentLoaded", function () {
  const usuarioInput = document.getElementById("signup");
  const contrasenaInput = document.getElementById("contrasena");
  const redirigirButton = document.getElementById("redirigir-button");
  const errorMessage = document.getElementById("error-message");

  function habilitarBoton() {
    if (usuarioInput.value && contrasenaInput.value) {
      redirigirButton.removeAttribute("disabled");
    } else {
      redirigirButton.setAttribute("disabled", "disabled");
    }
  }

  usuarioInput.addEventListener("input", habilitarBoton);
  contrasenaInput.addEventListener("input", habilitarBoton);

  redirigirButton.addEventListener("click", function () {
    if (
      !redirigirButton.hasAttribute("disabled") &&
      !errorMessage.textContent
    ) {
      window.open("main_login.html");
    }
  });

  contrasenaInput.addEventListener("keyup", validatePassword);

  function validatePassword() {
    const password = contrasenaInput.value;
    const hasUpperCase = /[A-Z]/.test(password);
    const hasLowerCase = /[a-z]/.test(password);
    const hasNumber = /[0-9]/.test(password);
    const hasSpecialChar = /[!@#$%^&*()_+[\]{};':"\\|,.<>/?-]/.test(password);

    if (hasUpperCase && hasLowerCase && hasNumber && hasSpecialChar) {
      errorMessage.textContent = "";
    } else {
      errorMessage.textContent =
        "⚠️ La contraseña debe contener al menos una mayúscula, una minúscula, un número y un carácter especial.";
    }
  }
});
