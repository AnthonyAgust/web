document.addEventListener("DOMContentLoaded", function () {
  const nombreInput = document.getElementById("nombre");
  const apellidoInput = document.getElementById("apellido");
  const cedulaInput = document.getElementById("cedula");
  const fechaNacimientoInput = document.getElementById("fecha_nacimiento");
  const ciudadInput = document.getElementById("ciudad");
  const direccionInput = document.getElementById("direccion");
  const celularInput = document.getElementById("celular");
  const correoInput = document.getElementById("correo");
  const passwordInput = document.getElementById("password"); // Campo de contraseña
  const confirmarContrasenaInput = document.getElementById(
    "confirmar_contrasena"
  ); // Campo de confirmar contraseña
  const botonIngresar = document.getElementById("boton");

  const nombreMensaje = document.getElementById("nombreMensaje");
  const apellidoMensaje = document.getElementById("apellidoMensaje");
  const cedulaMensaje = document.getElementById("cedulaMensaje");
  const fechaNacimientoMensaje = document.getElementById(
    "fechaNacimientoMensaje"
  );
  const ciudadMensaje = document.getElementById("ciudadMensaje");
  const direccionMensaje = document.getElementById("direccionMensaje");
  const celularMensaje = document.getElementById("celularMensaje");
  const correoMensaje = document.getElementById("correoMensaje");
  const passwordMensaje = document.getElementById("passwordMensaje"); // Mensaje de validación de contraseña
  const confirmarContrasenaMensaje = document.getElementById(
    "confirmarContrasenaMensaje"
  ); // Mensaje de validación de confirmar contraseña

  botonIngresar.style.display = "none";

  nombreInput.addEventListener("input", validarNombre);
  apellidoInput.addEventListener("input", validarApellido);
  cedulaInput.addEventListener("input", validarCedula);
  fechaNacimientoInput.addEventListener("input", validarFechaNacimiento);
  ciudadInput.addEventListener("input", validarCiudad);
  direccionInput.addEventListener("input", validarDireccion);
  celularInput.addEventListener("input", validarCelular);
  correoInput.addEventListener("input", validarCorreo);
  passwordInput.addEventListener("input", validarPassword); // Validación de contraseña
  confirmarContrasenaInput.addEventListener(
    "input",
    validarConfirmarContrasena
  ); // Validación de confirmar contraseña

  function verificarCamposVacios() {
    if (nombreInput.value === "") {
      nombreMensaje.innerText = "⚠️ Este campo no puede quedar en blanco";
    }

    if (apellidoInput.value === "") {
      apellidoMensaje.innerText = "⚠️ Este campo no puede quedar en blanco";
    }

    if (cedulaInput.value === "") {
      cedulaMensaje.innerText = "⚠️ Este campo no puede quedar en blanco";
    }

    if (fechaNacimientoInput.value === "") {
      fechaNacimientoMensaje.innerText =
        "⚠️ Este campo no puede quedar en blanco";
    }

    if (ciudadInput.value === "") {
      ciudadMensaje.innerText = "⚠️ Este campo no puede quedar en blanco";
    }

    if (direccionInput.value === "") {
      direccionMensaje.innerText = "⚠️ Este campo no puede quedar en blanco";
    }

    if (celularInput.value === "") {
      celularMensaje.innerText = "⚠️ Este campo no puede quedar en blanco";
    }

    if (correoInput.value === "") {
      correoMensaje.innerText = "⚠️ Este campo no puede quedar en blanco";
    }
  }

  verificarCamposVacios();

  function validarNombre() {
    const valor = nombreInput.value.trim();
    if (valor.length < 10) {
      nombreMensaje.innerText = "⚠️ Debe tener más de 10 caracteres";
    } else {
      nombreMensaje.innerText = "";
    }
    habilitarBotonIngresar();
  }

  function validarApellido() {
    const valor = apellidoInput.value.trim();
    if (valor.length < 10) {
      apellidoMensaje.innerText = "⚠️ Debe tener más de 10 caracteres";
    } else {
      apellidoMensaje.innerText = "";
    }
    habilitarBotonIngresar();
  }

  function validarCedula() {
    const valor = cedulaInput.value.trim();
    if (valor.length !== 10) {
      cedulaMensaje.innerText = "⚠️ Debe tener exactamente 10 números";
    } else {
      cedulaMensaje.innerText = "";
    }
    habilitarBotonIngresar();
  }

  function validarFechaNacimiento() {
    const valor = fechaNacimientoInput.value;
    if (!valor) {
      fechaNacimientoMensaje.innerText = "⚠️ Ingresa datos válidos";
    } else {
      const fechaNacimiento = new Date(valor);
      const hoy = new Date();
      const edad = hoy.getFullYear() - fechaNacimiento.getFullYear();

      if (edad > 90) {
        fechaNacimientoMensaje.innerText =
          "⚠️ Ingresa una fecha de nacimiento válida";
      } else {
        fechaNacimientoMensaje.innerText = "";
      }
    }
    habilitarBotonIngresar();
  }

  function validarCiudad() {
    const valor = ciudadInput.value.trim();
    if (valor === "") {
      ciudadMensaje.innerText = "⚠️ No puede quedar en blanco";
    } else {
      ciudadMensaje.innerText = "";
    }
    habilitarBotonIngresar();
  }

  function validarDireccion() {
    const valor = direccionInput.value.trim();
    if (valor === "") {
      direccionMensaje.innerText = "⚠️ No puede quedar en blanco";
    } else {
      direccionMensaje.innerText = "";
    }
    habilitarBotonIngresar();
  }

  function validarCelular() {
    const valor = celularInput.value.trim();
    if (valor.length !== 10) {
      celularMensaje.innerText = "⚠️ Debe tener exactamente 10 números";
    } else {
      celularMensaje.innerText = "";
    }
    habilitarBotonIngresar();
  }

  function validarCorreo() {
    const valor = correoInput.value.trim();
    if (valor === "") {
      correoMensaje.innerText = "⚠️ No puede quedar en blanco";
    } else {
      correoMensaje.innerText = "";
    }
    habilitarBotonIngresar();
  }

  function validarPassword() {
    const valor = passwordInput.value.trim();
    if (valor.length < 8) {
      passwordMensaje.innerText =
        "⚠️ La contraseña debe tener al menos 8 caracteres";
    } else {
      passwordMensaje.innerText = "";
    }
    habilitarBotonIngresar();
  }

  function validarRequisitosContrasena(contrasena) {
    const expresion =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/;
    return expresion.test(contrasena);
  }

  function validarPassword() {
    const valor = passwordInput.value.trim();

    if (valor.length < 8) {
      passwordMensaje.innerText =
        "⚠️ La contraseña debe tener al menos 8 caracteres";
    } else if (!validarRequisitosContrasena(valor)) {
      passwordMensaje.innerText =
        "⚠️ La contraseña debe contener al menos una letra mayúscula, una letra minúscula, un número y un carácter especial (@, $, !, %, *, ?, &)";
    } else {
      passwordMensaje.innerText = "";
    }
    habilitarBotonIngresar();
  }
  function validarConfirmarContrasena() {
    const contrasena = passwordInput.value.trim();
    const confirmarContrasena = confirmarContrasenaInput.value.trim();

    if (contrasena !== confirmarContrasena) {
      confirmarContrasenaMensaje.innerText = "⚠️ Las contraseñas no coinciden";
    } else {
      confirmarContrasenaMensaje.innerText = "";
    }
    habilitarBotonIngresar();
  }

  function habilitarBotonIngresar() {
    const camposCompletos =
      nombreMensaje.innerText === "" &&
      apellidoMensaje.innerText === "" &&
      cedulaMensaje.innerText === "" &&
      fechaNacimientoMensaje.innerText === "" &&
      ciudadMensaje.innerText === "" &&
      direccionMensaje.innerText === "" &&
      celularMensaje.innerText === "" &&
      correoMensaje.innerText === "" &&
      passwordMensaje.innerText === "" &&
      confirmarContrasenaMensaje.innerText === "";

    if (camposCompletos) {
      botonIngresar.style.display = "block";
    } else {
      botonIngresar.style.display = "none";
    }
  }

  botonIngresar.addEventListener("click", function (event) {
    if (botonIngresar.style.display === "none") {
      event.preventDefault();
    }
  });

  botonIngresar.addEventListener("click", function (event) {
    if (botonIngresar.style.display === "block") {
      window.location.href = "main_login.html";
    }
  });
});
