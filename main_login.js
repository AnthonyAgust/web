document.addEventListener("DOMContentLoaded", function () {
  const formularioAlumnos = document.getElementById("formulario-alumnos");
  const toggleAlumnos = document.getElementById("toggle-alumnos");
  const formularioDocentes = document.getElementById("formulario-docentes");
  const toggleDocentes = document.getElementById("toggle-docentes");
  const formularioAsignaturas = document.getElementById(
    "formulario-asignaturas"
  );
  const toggleAsignaturas = document.getElementById("toggle-asignaturas");
  const formularioPersonal = document.getElementById("formulario-personal");
  const togglePersonal = document.getElementById("toggle-personal");

  toggleAlumnos.addEventListener("click", function () {
    if (
      formularioAlumnos.style.display === "none" ||
      formularioAlumnos.style.display === ""
    ) {
      formularioAlumnos.style.display = "block";
      formularioAlumnos.style.opacity = "1";
      formularioDocentes.style.display = "none";
      formularioDocentes.style.opacity = "0";
      formularioAsignaturas.style.display = "none";
      formularioAsignaturas.style.opacity = "0";
      formularioPersonal.style.display = "none";
      formularioPersonal.style.opacity = "0";
    } else {
      formularioAlumnos.style.display = "none";
      formularioAlumnos.style.opacity = "0";
    }
  });

  toggleDocentes.addEventListener("click", function () {
    if (
      formularioDocentes.style.display === "none" ||
      formularioDocentes.style.display === ""
    ) {
      formularioDocentes.style.display = "block";
      formularioDocentes.style.opacity = "1";
      formularioAlumnos.style.display = "none";
      formularioAlumnos.style.opacity = "0";
      formularioAsignaturas.style.display = "none";
      formularioAsignaturas.style.opacity = "0";
      formularioPersonal.style.display = "none";
      formularioPersonal.style.opacity = "0";
    } else {
      formularioDocentes.style.display = "none";
      formularioDocentes.style.opacity = "0";
    }
  });

  toggleAsignaturas.addEventListener("click", function () {
    if (
      formularioAsignaturas.style.display === "none" ||
      formularioAsignaturas.style.display === ""
    ) {
      formularioAsignaturas.style.display = "block";
      formularioAsignaturas.style.opacity = "1";
      formularioAlumnos.style.display = "none";
      formularioAlumnos.style.opacity = "0";
      formularioDocentes.style.display = "none";
      formularioDocentes.style.opacity = "0";
      formularioPersonal.style.display = "none";
      formularioPersonal.style.opacity = "0";
    } else {
      formularioAsignaturas.style.display = "none";
      formularioAsignaturas.style.opacity = "0";
    }
  });

  togglePersonal.addEventListener("click", function () {
    if (
      formularioPersonal.style.display === "none" ||
      formularioPersonal.style.display === ""
    ) {
      formularioPersonal.style.display = "block";
      formularioPersonal.style.opacity = "1";
      formularioAlumnos.style.display = "none";
      formularioAlumnos.style.opacity = "0";
      formularioDocentes.style.display = "none";
      formularioDocentes.style.opacity = "0";
      formularioAsignaturas.style.display = "none";
      formularioAsignaturas.style.opacity = "0";
    } else {
      formularioPersonal.style.display = "none";
      formularioPersonal.style.opacity = "0";
    }
  });
});
